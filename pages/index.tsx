import type { GetStaticProps, NextPage } from "next";
import { PrismaClient, Set } from "@prisma/client";
import Link from "next/link";
import Layout from "../components/Layout";
import PageHeading from "../components/PageHeading";
import {
  PlusSmIcon as PlusSmIconSolid,
  ChevronRightIcon
} from "@heroicons/react/solid";
import { Popover } from "@headlessui/react";
import { useRef } from "react";

interface Props {
  sets: Set[];
}

const Home: NextPage<Props> = ({ sets }) => {
  const createForm = useRef<HTMLFormElement>(null);
  const createNewSet = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (createForm.current) {
      const formData = new FormData(createForm.current);
      const name = formData.get("name") as string;
      const description = formData.get("description") as string;
      const set = {
        data: {
          name,
          description
        }
      };
      const response = await fetch("/api/createSet", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(set)
      }).then(() => {
        createForm.current?.querySelectorAll("input").forEach((input) => {
          input.value = "";
        });
        createForm.current?.querySelectorAll("textarea").forEach((textarea) => {
          textarea.value = "";
        });
      });
    }
  };

  return (
    <Layout>
      <div className="pb-5 flex items-center justify-between border-b border-gray-200">
        <div>
          <PageHeading>Välj ämne</PageHeading>
          <p className="mt-2 max-w-3xl text-sm text-gray-500">
            Välj det ämne du vill öva på
          </p>
        </div>
        <div>
          <Popover className="relative">
            <Popover.Button
              type="button"
              className="inline-flex items-center p-1.5 border border-transparent rounded-full shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              <PlusSmIconSolid className="h-5 w-5" aria-hidden="true" />
            </Popover.Button>
            <Popover.Panel className="absolute w-64 max-w-xs right-0 mt-3 z-10 bg-white overflow-hidden shadow rounded-lg">
              <form
                ref={createForm}
                onSubmit={createNewSet}
                className="px-4 py-5 sm:p-6 grid gap-3"
              >
                <div>
                  <label
                    htmlFor="name"
                    className="block text-sm font-medium text-gray-700"
                  >
                    Namn
                  </label>
                  <div className="mt-1">
                    <input
                      type="text"
                      name="name"
                      id="name"
                      className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                    />
                  </div>
                </div>
                <div>
                  <label
                    htmlFor="description"
                    className="block text-sm font-medium text-gray-700"
                  >
                    Beskrivning
                  </label>
                  <div className="mt-1">
                    <textarea
                      name="description"
                      id="description"
                      className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                      rows={4}
                    ></textarea>
                  </div>
                </div>
                <div>
                  <button
                    type="submit"
                    className="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                  >
                    Spara
                  </button>
                </div>
              </form>
            </Popover.Panel>
          </Popover>
        </div>
      </div>
      <div className="bg-white shadow overflow-hidden rounded-md mt-6">
        <ul role="list" className="divide-y divide-gray-200">
          {sets.map((set) => (
            <li key={set.id}>
              <Link href={`/set/${set.uuid}`}>
                <a className="px-6 py-4 text-gray-700 group flex items-center justify-between">
                  <span>{set.name}</span>
                  <ChevronRightIcon
                    className="h-5 w-5 text-gray-300 transition-colors group-hover:text-gray-700"
                    aria-hidden="true"
                  />
                </a>
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  const prisma = new PrismaClient();
  const sets = await prisma.set.findMany();
  return {
    props: {
      sets: JSON.parse(JSON.stringify(sets))
    }
  };
};

export default Home;
