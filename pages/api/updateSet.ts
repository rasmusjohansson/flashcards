// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { PrismaClient } from "@prisma/client";
import type { NextApiRequest, NextApiResponse } from "next";

type Data = {
  name: string;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const prisma = new PrismaClient();
  const set = await prisma.set.update({
    where: {
      id: parseInt(req.body.data.id)
    },
    data: {
      name: req.body.data.name,
      description: req.body.data.description
    }
  });

  res.status(200).json(set);
}
