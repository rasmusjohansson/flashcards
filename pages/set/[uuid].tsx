import type { GetStaticProps, GetStaticPaths, NextPage } from "next";
import { Flashcard, PrismaClient, Set } from "@prisma/client";
import { useRef, useState } from "react";
import Layout from "../../components/Layout";
import PageHeading from "../../components/PageHeading";
import { CogIcon, PlusSmIcon } from "@heroicons/react/solid";
import { Popover } from "@headlessui/react";
import { TrashIcon } from "@heroicons/react/outline";

interface Props {
  set: Set;
  cards: Flashcard[];
}

const SetPage: NextPage<Props> = ({ set, cards }) => {
  const updateSetForm = useRef<HTMLFormElement>(null);
  const createQuestionForm = useRef<HTMLFormElement>(null);

  const createQuestion = async (event: any) => {
    event.preventDefault();
  };

  const updateSet = async (event: any) => {
    event.preventDefault();
    if (updateSetForm.current) {
      const formData = new FormData(updateSetForm.current);
      const id = formData.get("id") as string;
      const name = formData.get("name") as string;
      const description = formData.get("description") as string;
      const set = {
        data: {
          id,
          name,
          description
        }
      };
      const response = await fetch("/api/updateSet", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(set)
      });
    }
  };

  return (
    <Layout>
      <div className="pb-5 flex items-center justify-between border-b border-gray-200">
        <div>
          <PageHeading>{set.name}</PageHeading>
          <p className="mt-2 max-w-3xl text-sm text-gray-500">
            {set.description}
          </p>
        </div>
        <div className="flex items-center space-x-3">
          <Popover className="relative">
            <Popover.Button
              type="button"
              className="inline-flex items-center p-1.5 border border-transparent rounded-full shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              <CogIcon className="h-5 w-5" aria-hidden="true" />
            </Popover.Button>
            <Popover.Panel className="absolute w-64 max-w-xs right-0 mt-3 z-10 bg-white overflow-hidden shadow rounded-lg">
              <form
                ref={updateSetForm}
                onSubmit={updateSet}
                className="px-4 py-5 sm:p-6 grid gap-3"
              >
                <div>
                  <label
                    htmlFor="name"
                    className="block text-sm font-medium text-gray-700"
                  >
                    Namn
                  </label>
                  <div className="mt-1">
                    <input
                      type="text"
                      name="name"
                      id="name"
                      defaultValue={set.name}
                      className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                    />
                  </div>
                </div>
                <div>
                  <label
                    htmlFor="description"
                    className="block text-sm font-medium text-gray-700"
                  >
                    Beskrivning
                  </label>
                  <div className="mt-1">
                    <textarea
                      name="description"
                      id="description"
                      className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                      rows={4}
                      defaultValue={set.description}
                    ></textarea>
                  </div>
                </div>
                <div>
                  <input
                    type="hidden"
                    name="id"
                    id="id"
                    defaultValue={set.id}
                  />
                  <button
                    type="submit"
                    className="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                  >
                    Spara
                  </button>
                </div>
              </form>
            </Popover.Panel>
          </Popover>
          <Popover className="relative">
            <Popover.Button
              type="button"
              className="inline-flex items-center p-1.5 border border-transparent rounded-full shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              <PlusSmIcon className="h-5 w-5" aria-hidden="true" />
            </Popover.Button>
            <Popover.Panel className="absolute w-64 max-w-xs right-0 mt-3 z-10 bg-white overflow-hidden shadow rounded-lg">
              <form
                ref={createQuestionForm}
                onSubmit={createQuestion}
                className="px-4 py-5 sm:p-6 grid gap-3"
              >
                <div>
                  <label
                    htmlFor="question"
                    className="block text-sm font-medium text-gray-700"
                  >
                    Fråga
                  </label>
                  <div className="mt-1">
                    <input
                      type="text"
                      name="question"
                      id="question"
                      className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                    />
                  </div>
                </div>
                <div>
                  <label
                    htmlFor="answer"
                    className="block text-sm font-medium text-gray-700"
                  >
                    Svar
                  </label>
                  <div className="mt-1">
                    <textarea
                      name="answer"
                      id="answer"
                      className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                      rows={4}
                    ></textarea>
                  </div>
                </div>
                <div>
                  <input type="hidden" name="setId" value={set.id} />
                  <button
                    type="submit"
                    className="inline-flex items-center px-3 py-2 border border-transparent text-sm leading-4 font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                  >
                    Spara
                  </button>
                </div>
              </form>
            </Popover.Panel>
          </Popover>
        </div>
      </div>
      <div className="mt-6">
        <ul className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-6">
          {cards.map((card) => (
            <Card key={card.id} card={card} />
          ))}
        </ul>
      </div>
    </Layout>
  );
};

const Card = ({ card }: { card: Flashcard }) => {
  const [showAnswer, setShowAnswer] = useState(false);
  return (
    <li
      className="relative bg-white overflow-hidden shadow group rounded-lg px-8 py-16 text-center grid items-center cursor-pointer"
      key={card.id}
    >
      {/* <button className="absolute top-0 z-10 right-0 mt-2 mr-2 p-2 transition-opacity rounded-full opacity-0 bg-red-50 group-hover:opacity-100">
        <TrashIcon aria-hidden="true" className="h-5 w-5 text-red-700" />
      </button> */}
      <div
        className={`col-span-1 col-start-1 row-span-1 row-start-1 ${
          showAnswer && "opacity-0"
        }`}
        onClick={() => setShowAnswer(!showAnswer)}
      >
        <div className="font-semibold">Fråga:</div>
        <div>{card.question}</div>
      </div>
      <div
        className={`col-span-1 col-start-1 row-span-1 row-start-1 ${
          !showAnswer && "opacity-0"
        }`}
        onClick={() => setShowAnswer(!showAnswer)}
      >
        <div className="font-semibold">Svar:</div>
        <div>{card.answer}</div>
      </div>
    </li>
  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  const prisma = new PrismaClient();
  const currentSet = await prisma.set.findUnique({
    where: {
      uuid: context.params?.uuid as string | undefined
    }
  });
  const cardsForSet = await prisma.flashcard.findMany({
    where: {
      setId: currentSet!.id as number | undefined
    }
  });
  return {
    props: {
      set: JSON.parse(JSON.stringify(currentSet)),
      cards: JSON.parse(JSON.stringify(cardsForSet))
    }
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  const prisma = new PrismaClient();
  const sets = await prisma.set.findMany();

  const paths = sets.map((set) => ({
    params: { uuid: set.uuid }
  }));
  return {
    paths,
    fallback: false
  };
};

export default SetPage;
