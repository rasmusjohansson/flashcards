import Head from "next/head";
import NavBar from "./Navbar";

export default function Layout({ children }: { children: React.ReactNode }) {
  return (
    <div className="bg-gray-50 font-sans text-gray-900 antialiased min-h-screen">
      <Head>
        <title>Flashcards</title>
        <meta name="description" content="" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <NavBar />
      <main>
        <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 py-8 sm:py-12 lg:py-16">
          {children}
        </div>
      </main>
    </div>
  );
}
