import React from "react";

export default function PageHeading({
  children
}: {
  children: React.ReactChild;
}) {
  return (
    <h1 className="text-2xl font-bold leading-7 text-gray-900 sm:text-3xl sm:truncate">
      {children}
    </h1>
  );
}
